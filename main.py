from transformers import AutoModelForCausalLM, AutoTokenizer
from flask import Flask, request, jsonify, render_template, session
from markdown import markdown
from flask_cors import CORS
import requests
import os
import uuid

app = Flask(__name__)
CORS(app)

# Set the secret key for the application
app.secret_key = "caXDjH42lHGzuDW9cPXO"

checkpoint = "bigscience/bloomz-1b1"
model_dir = "model"

if not os.path.exists(model_dir):
    print("Model checkpoint not found. Downloading from bigscience...")
    os.makedirs(model_dir, exist_ok=True)
    try:
        model = AutoModelForCausalLM.from_pretrained(checkpoint, cache_dir=model_dir, resume_download=True)
        print("The model was downloaded partially. Resuming...")
    except Exception:
        model = AutoModelForCausalLM.from_pretrained(checkpoint, cache_dir=model_dir)

tokenizer = AutoTokenizer.from_pretrained(checkpoint, cache_dir=model_dir)
model = AutoModelForCausalLM.from_pretrained(checkpoint, cache_dir=model_dir)

app.jinja_env.filters['markdown'] = lambda text: markdown(text, output_format='html5')

# Create a dictionary to store the threads
threads = {}
# Create a list for messages
messages = []

@app.route('/')
def index():
    # Clear the current session
    session.clear()

    # Clear the thread dictionary
    threads.clear()

    # Clear the messages list
    messages.clear()

    return render_template('index.html', messages=messages)


@app.route('/chat', methods=['POST'])
def chat():
    data = request.get_json()
    messages.append({'user': 'You', 'text': data['message']})
    messages.append({'user': 'Model', 'text': 'Model is generating a response...'})

    # Retrieve the thread id from the session
    thread_id = session.get('thread_id')
    if thread_id is None:
        # Generate a unique thread id if it doesn't exist
        thread_id = uuid.uuid4()
        session['thread_id'] = thread_id

    # Retrieve the thread from the dictionary
    thread = threads.get(thread_id)
    if thread is None:
        # Create a new thread if it doesn't exist
        thread = []
        threads[thread_id] = thread

    # Append the new message to the thread
    thread.append(data['message'])

    # Encode the previous messages in the thread as input
    prompt_text = "\n".join(thread)
    inputs = tokenizer.encode(prompt_text, return_tensors="pt")
    num_tokens = len(inputs[0])
    if num_tokens < 500:
        max_new_tokens = 500
    else:
        max_new_tokens = num_tokens

    # Initialize outputs to an empty list
    outputs = []

    # Generate a response using the previous messages as context
    outputs = model.generate(inputs, max_new_tokens=max_new_tokens)
    response = {'message': tokenizer.decode(outputs[0])}
    messages.append({'user': 'Model', 'text': response['message']})
    return jsonify(response)


if __name__ == '__main__':
    app.run()
